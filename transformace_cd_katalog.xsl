<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>XSL transformace</title>
        <link href="./transformace_cd_katalog.css" rel="stylesheet" />
      </head>
      <body>
        <h1>Sbírka CD</h1>
        <h2>Moje sbírka raritních CD</h2>
        <xsl:apply-templates />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="cd">
    <section>
      <xsl:attribute name="id">
        <xsl:value-of select="./@id" />
      </xsl:attribute>
      <h3>
        <xsl:if test="@favorite = 1">
          <xsl:attribute name="class">favourite</xsl:attribute>
        </xsl:if>
        <span class="cd-name">
          <xsl:value-of select="titul" />
        </span>
        /
        <span class="cd-artist">
          <xsl:value-of select="umelec" />
        </span>
      </h3>

      <xsl:if test="@favorite = 1">
        <div class="favourite info">moje oblíbené CD</div>
      </xsl:if>

      <ul>
        <li>
          Vydala společnost
          <xsl:value-of select="spolecnost" />
          v roce
          <xsl:value-of select="vydano" />
        </li>
        <xsl:apply-templates select="stat" />
        <xsl:apply-templates select="cena" />
      </ul>
    </section>
  </xsl:template>

  <xsl:template match="stat">
    <li>
      Vydáno v
      <xsl:value-of select="." />
    </li>
  </xsl:template>

  <xsl:template match="cena">
    <li>
      Cena za desku:
      <xsl:value-of select="." />
      <xsl:text> </xsl:text>
      <xsl:value-of select="./@mena" />
    </li>
  </xsl:template>
</xsl:stylesheet>
